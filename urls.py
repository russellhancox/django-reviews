from django.conf.urls.defaults import *
from reviews.models import Review

urlpatterns = patterns('django.views.generic.list_detail',
	(r'^$', 'object_list', {'queryset': Review.objects.all(), 'template_object_name': 'review'}), 
	(r'^(?P<object_id>\d+)/$', 'object_detail', {'queryset': Review.objects.all(), 'template_object_name': 'review'}),
	(r'^(?P<slug>[-\w]+)/$', 'object_detail', {'queryset': Review.objects.all(), 'template_object_name': 'review'}),
)
