django-reviews
==============

A simple reviewing application for Django featuring configurable categories and product images

* * *

Requirements
------------

Django 1.2+

May work with earlier versions, but hasn't been tested

South (optional)

If the models are changed in the future, South can be used to migrate the data.

Installation
------------

1. Download the code, put it into your project's directory
2. Add 'reviews' to the INSTALLED_APPS variable
3. Run manage.py syncdb to add the required tables to your database
4. Include the urls in your project:

	(r'reviews/', include('reviews.urls')),

* * *

Settings
-------

REVIEWS_UPLOAD_TO

This setting determines where under MEDIA_ROOT the uploaded images are stored. Defaults to review-images

* * *

Templates
---------

The app uses Django built-in views as it's not very complicated. The included templates extend base.html and add to the 'title' and 'content' blocks.

If your project isn't setup like this, or you wish to change the templates (not unreasonable, they're very basic), make a copy of the included templates
in your project's template directory.

* * *

Known Issues
------------

Once an image has been uploaded for a review, it's only possible to change it, not remove it. The latest (currently dev) version of Django fixes this. 


