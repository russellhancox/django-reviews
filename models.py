from django.db import models
from django.db.models import permalink
from django.conf import settings
from django.core.validators import MaxValueValidator

class ReviewCategory(models.Model):
        name = models.CharField(max_length=100)

	class Meta:
		verbose_name_plural = "Review Categories"
        
	def __unicode__(self):
	        return self.name

class Review(models.Model):
	title = models.CharField(max_length=300)
	slug = models.SlugField()
	pubdate = models.DateTimeField('date published', auto_now_add=True)
	comments = models.TextField()
	rating = models.PositiveSmallIntegerField(validators=[MaxValueValidator(10)], help_text="Between 0-10")
	category = models.ForeignKey(ReviewCategory)


	if hasattr(settings, 'REVIEWS_UPLOAD_TO'):
		image = models.ImageField(upload_to=settings.REVIEWS_UPLOAD_TO, null=True, blank=True, help_text="Optional")
	else:
		image = models.ImageField(upload_to='review-images/', null=True, blank=True, help_text="Optional")

	def __unicode__(self):
		return self.title

	@permalink
	def get_absolute_url(self):
		return ('django.views.generic.list_detail.object_detail', [self.slug]) 
