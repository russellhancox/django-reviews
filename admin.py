from reviews.models import Review, ReviewCategory
from django.contrib import admin

class ReviewAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("title",)}
	list_display = ('title', 'rating', 'category', 'pubdate')
	list_filter = ['category', 'pubdate']
	search_fields = ['title']

admin.site.register(Review, ReviewAdmin)
admin.site.register(ReviewCategory)
