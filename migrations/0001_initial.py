# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'ReviewCategory'
        db.create_table('reviews_reviewcategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('reviews', ['ReviewCategory'])

        # Adding model 'Review'
        db.create_table('reviews_review', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, db_index=True)),
            ('pubdate', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')()),
            ('rating', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reviews.ReviewCategory'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('reviews', ['Review'])


    def backwards(self, orm):
        
        # Deleting model 'ReviewCategory'
        db.delete_table('reviews_reviewcategory')

        # Deleting model 'Review'
        db.delete_table('reviews_review')


    models = {
        'reviews.review': {
            'Meta': {'object_name': 'Review'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['reviews.ReviewCategory']"}),
            'comments': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pubdate': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'rating': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'reviews.reviewcategory': {
            'Meta': {'object_name': 'ReviewCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['reviews']
